import { Module } from '@nestjs/common';
import {TrackController} from "./track.controller";
import {MongooseModule} from "@nestjs/mongoose";
import {Track, TrackSchema} from "./schemas/track.schema";
import {CommentSchema} from "./schemas/comment.schema";
import {TrackService} from "./track.servise";
import {FileService} from "../file/file.servise";
import {Comment} from "./schemas/comment.schema";

@Module({
    imports: [
        MongooseModule.forFeature([{name: Track.name, schema: TrackSchema}]),
        MongooseModule.forFeature([{name: Comment.name, schema: CommentSchema}]),
    ],
    controllers: [TrackController],
    providers: [TrackService,FileService],
})
export class TrackModule {}